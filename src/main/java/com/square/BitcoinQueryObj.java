package com.square;

import java.util.ArrayList;

import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;

public class BitcoinQueryObj {

	public static ArrayList<BitcoinData> generateBean(TableResult payload) {

		ArrayList<BitcoinData> data = new ArrayList<BitcoinData>();
		for (FieldValueList row : payload.iterateAll()) {
			BitcoinData dt = new BitcoinData();
			dt.setActor_balance_token(row.get("actor_balance_token").getStringValue());
			dt.setActor_currency_code(row.get("actor_currency_code").getStringValue());
			dt.setTransaction_type(row.get("transaction_type").getStringValue());
			dt.setCreated_at(row.get("created_at").getLongValue());
			dt.setActor_amount(row.get("actor_amount").getLongValue());
			dt.setTransaction_reference(row.get("transaction_reference").getStringValue());
			dt.setSource_id(row.get("source_id").getStringValue());
			
			dt.setOriginator(row.get("originator").getStringValue());
			
			
			if (!(row.get("recipient_amount").isNull()))
				dt.setRecipient_amount(row.get("recipient_amount").getLongValue());

			//dt.setRecipient_currency_code(row.get("recipient_currency_code").getStringValue());
			if (!(row.get("recipient_currency_code").isNull()))
				dt.setRecipient_currency_code(row.get("recipient_currency_code").getStringValue());
			
			data.add(dt);

		}
		return data;
	}


}
