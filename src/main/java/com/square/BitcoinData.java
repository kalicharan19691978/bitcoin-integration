package com.square;

import java.io.Serializable;

public class BitcoinData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String actor_currency_code, actor_balance_token, transaction_type, transaction_reference, source_id, originator, recipient_currency_code;
	Long created_at,  actor_amount, recipient_amount;

	public String getActor_currency_code() {
		return actor_currency_code;
	}
	public void setActor_currency_code(String actor_currency_code) {
		this.actor_currency_code = actor_currency_code;
	}
	public String getActor_balance_token() {
		return actor_balance_token;
	}
	public void setActor_balance_token(String actor_balance_token) {
		this.actor_balance_token = actor_balance_token;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	public void setTransaction_reference(String transaction_reference) {
		this.transaction_reference = transaction_reference;
	}
	public String getSource_id() {
		return source_id;
	}
	public void setSource_id(String source_id) {
		this.source_id = source_id;
	}
	public Long getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Long created_at) {
		this.created_at = created_at;
	}
	public Long getActor_amount() {
		return actor_amount;
	}
	public void setActor_amount(Long actor_amount) {
		this.actor_amount = actor_amount;
	}
	public Long getRecipient_amount() {
		return recipient_amount;
	}
	public void setRecipient_amount(Long recipient_amount) {
		this.recipient_amount = recipient_amount;
	}
	public String getTransaction_reference() {
		return transaction_reference;
	}
	public String getOriginator() {
		return originator;
	}
	public void setOriginator(String originator) {
		this.originator = originator;
	}
	public String getRecipient_currency_code() {
		return recipient_currency_code;
	}
	public void setRecipient_currency_code(String recipient_currency_code) {
		this.recipient_currency_code = recipient_currency_code;
	}
	
}
